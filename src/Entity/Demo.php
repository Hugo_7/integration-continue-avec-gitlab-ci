<?php

namespace App\Entity;

use App\Repository\DemoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemoRepository::class)
 */
class Demo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dedemo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDedemo(): ?string
    {
        return $this->dedemo;
    }

    public function setDedemo(string $dedemo): self
    {
        $this->dedemo = $dedemo;

        return $this;
    }
}
